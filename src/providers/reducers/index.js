import isLoggedReducer from "./isLoggedIn.js";

import { combineReducers } from "redux";

const allReducers = combineReducers({
  isLogged: isLoggedReducer,
});

export default allReducers;
